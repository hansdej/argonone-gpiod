# installing
After cloning this repo to your (Debian) raspberry pi with Argon One case
you can genereate the `deb` package file by running the `build_deb` script.

# Some functionality
The configuration resides in `/etc/argonone_cfg.json`.
That Contains the fan speeds and sets the number of the Argon one I2C host.
On my Debian Bullseye/bookworm installation I found it to be `3`.

On Raspbian systems this seems not to be the case, but since Raspbian might not have adopted
the modern `gpiod`-driver yet, you need other driving software than this.

# Good luck
"It works on my system." No garantee about yours, but I hope the code is simple enough to build
upon further if you need.

Hans
