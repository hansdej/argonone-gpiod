#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module docstring goes here"""
import sys

__author__ = 'Hans de Jonge'
__version__ = ''
__date__ = ''
__license__ = 'Gpl'

import logging
logging.basicConfig(
    format='%(module)s:%(lineno)d: %(message)s',
    level=logging.INFO
)
logger = logging.getLogger('')


def main(args):
    """The main function is the entry point of a program"""
    # main is separated out into a function so that it too can be thoroughly
    # tested.
    logger.debug("Debug message." )
    logger.info("Info message." )
    logger.warning("Warning message." )
    logger.error("Error message." )
    logger.critical("Critical message." )
    return 0

if __name__ == '__main__':
    # This is the main body of this module. Ideally it should only contain at
    # most **one** call to the entry point of the program.
    sys.exit(main(sys.argv))

