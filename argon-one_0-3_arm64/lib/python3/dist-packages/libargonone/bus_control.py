#!/usr/bin/python3
"""
Control the I2C stuff of the Argon one:
    Fan & power cut.
"""

import smbus
import os
import sys
import time
from libargonone.do_logging import logger
from libargonone.do_config import my_argon_i2c_host
from libargonone.do_config import configuration, config_file


class argonone_i2c_control():
    """
    The fan control object.
    * Interacts with the configuration module.
    * Select fan speed based on temperature and configuration list.
    * Communicates with the I2C host.
    """
    fan_address = 0x1a
    i2c_host    = my_argon_i2c_host

    def __init__(self, **kwargs):
        """
        Basic setup
        """
        self.read_configuration()

    def set_i2c_host(self, hostnumber):
        self.i2c_host = hostnumber

    def read_configuration(self):
        """
        Obtain the configuration as determined in the do_config module.
        """
        cf = configuration(os.path.realpath(config_file))
        cf.read_config()

        if 'i2c_host' in cf.config:
            self.set_i2c_host(cf.config['i2c_host' ])

        # Continue with the fanspeeds:
        config_speeds = cf.config['fan_speeds']
        speeds = []
        for [condition, speed] in config_speeds:
            print(condition, speed)

            if condition == 'default':
                # Take out the default speed.
                self.default_speed = speed
            elif isinstance(condition, float):
                logger.debug("Adding t {}=> speed: {}".format(
                    condition, speed))

                speeds.append([condition, speed])
            else:
                logger.warning(
                    "Invalid temperature definition " +
                    " '{}'".format(condition) +
                    " ignoring this." )
        speeds.sort(key=lambda x: x[0])
        self.fan_speeds = speeds

    def lookup_fan_speed(self):
        """
        Determine whether the fanspeed needs to be changed from the temperature
        and the fanspeed configuration list.
        """
        sys_temp = self.get_sys_temperature()

        for temp, fan_speed in self.fan_speeds[::-1]:
            # Needs  a list sorted with decreasing temperature.
            logger.debug( "temp: %f" % temp)
            if sys_temp >= temp:
                return fan_speed
        return 0

    def get_sys_temperature(self):
        """
        Get the temperature as reported by the system.
        """
        with open("/sys/class/thermal/thermal_zone0/temp", "r") as fp:
            temp = fp.readline()
        sys_t = float(int(temp) / 1000)
        logger.debug("sys_t = {}".format(sys_t))
        return sys_t

    def cut_power(self):
        self.write_to_bus(0xFF)

    def write_fan_speed(self, speed):
        self.write_to_bus(speed)

    def write_to_bus(self, value):
        try:
            bus = smbus.SMBus(self.i2c_host)
            logger.debug("Writing {} to address {} in i2c-{}".format(
                value, self.fan_address, self.i2c_host))
            bus.write_byte(self.fan_address, value)
        except IOError as e:
            logger.warning( "IOError:"
                    + " '{}' when trying to write new fan velocity.".format(e))
            logger.warning("Tried writing {} to address {} in i2c-{}".format(
                value, self.fan_address, self.i2c_host))

    def temp_check_cycle(self, sync_time_s=30):
        """
        Main function to try to control the fan speed. Depending on the
        temperature detected by the pi's built in systen sensor.
        """
        prev_speed = 0

        while True:
            if hasattr(self, 'testing' ):
                # Enable config update only at test time:
                # Makes it easier to test temperature response in a system with
                # stable T.
                # Conventionally Linux systemd programs seem to work with
                # restarts between configuration changes.
                self.read_configuration()
            speed = self.lookup_fan_speed()

            if speed < prev_speed:
                # Sleep an extra sync_time_s seconds before throtling back.
                min_wait_s = 10
                wait_t_s = sync_time_s

                if wait_t_s < min_wait_s:
                    wait_t_s = min_wait_s
                time.sleep(wait_t_s)

            prev_speed = speed

            if hasattr(self, 'testing' ):
                print("Write fanspeed: {}".format(speed))
            else:
                self.write_fan_speed(speed)
            time.sleep(sync_time_s)


def fan_daemon():
    f_control = argonone_i2c_control()
    f_control.temp_check_cycle(sync_time_s=10)


def cut_power():
    bus_control = argonone_i2c_control()
    bus_control.cut_power()


def main(args):
    from threading import Thread
    try:
        t1 = Thread(target=fan_daemon)
        t1.start()

    except Exception as e:
        logger.critical(" Exception '{}' occured,".format(e) +
                       " trying to stop {}.".format(t1))
        t1.stop()


if __name__ == '__main__':
    sys.exit(main(sys.argv))

