#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Regulate the configuration of the fan and power buttoncontrol.

As the above line shows, there are two, mostly independent responsabilities. So
we should keep them apart as much as possible here also.
"""
import sys
import os
import json

__author__ = 'Hans de Jonge'
__version__ = ''
__date__ = ''
__license__ = 'Gpl'

from libargonone.do_logging import logger

cfg_filename = 'argonone_cfg.json'
config_file = os.path.join('/', 'etc', cfg_filename)

# For some reason there are 4 i2c controllers on my system and the ArgonOne is
# the last one.

my_argon_i2c_host = 3
default_config = {'i2c_host': my_argon_i2c_host,
                  'fan_speeds': [['default',   0],
                                    [ 50.0,  25],
                                    [ 55.0,  50],
                                    [ 65.0, 100]]}


class config_item():
    """
    A configuration item that we can assign al kinds of properties to.
    """
    def __init__(self, **kwargs):
        for prop, val in kwargs.items():
            setattr(self, prop, val)


def test_range(answer, wanted_type, lower, upper):
    try:
        param = wanted_type(answer)
    except ValueError:
        param = False

    if param is not False and param <= upper and param >= lower:
        logger.debug("Valid %r." % wanted_type)
    else:
        logger.debug("Invalid %r" % answer)
        param = False

    return param


class configuration():
    """
    A configurationclass.
    """
    def __init__(self, alternate_config_file=None):
        """
        Initialise the configuration object.
        """
        self.config_file = config_file
        self.config = { }

        if alternate_config_file is not None:
            self.config_file = alternate_config_file

    def read_config(self):
        """
        Return the content of the config file as a dictionary.
        """
        with open(self.config_file) as cf:
            self.config = json.load(cf)

    def add_i2c_host(self, i2c_host=None):
        """
        Add a configuration of the i2c host-number.
        i2c_host: if None, the default will be used.
        """
        if i2c_host is None:
            i2c_host = my_argon_i2c_host
        self.config['i2c_host'] = i2c_host

    def replace_fan_config(self, temp_speeds_list=None):
        """
        """
        if temp_speeds_list is None:
            temp_speeds_list = default_config['fan_speeds']
        self.config['fan_speeds'] = temp_speeds_list

    def write_config(self):
        """
        Write the configuration to the selected config_file.
        """
        fn = os.path.realpath(self.config_file)
        logger.debug("Write config to {}".format(fn))

        with open(fn, 'w' ) as cf:
            json.dump( self.config, cf, indent=4)
        logger.debug("Wrote config to {}".format(fn))

    def i2c_host_configuration_dialog(self):
        """
        """
        i2c_host = my_argon_i2c_host
        start_screen  = "\n\t--------------------------------"
        start_screen += "\n\tArgon One i2c host configurator."
        start_screen += "\n\t--------------------------------"
        start_screen += "\n\tReconfigure the number of the i2c"
        start_screen += "\n\thost on your system."
        start_screen += "\n\tDefaults to {}".format(my_argon_i2c_host)
        print(start_screen)

        valid_host_number = False
        while valid_host_number is False:
            reply = input( "\tPlease select the correct integer"
                        + "\n\tnumber of the ArgonOne i2c host on "
                        + "\n\tyour system:"
                        + "\n\t[input]> ")
            new_host = test_range( reply, int, 0, 64)
            if new_host is False:
                logger.error("Reply {} is".format(reply)
                             + " invalid, try again.")
            else:
                i2c_host = new_host
                valid_host_number = True

        self.add_i2c_host(i2c_host)

    def fan_configuration_dialogs(self):
        """
        Execute the configuration interaction dialogs sequence.
        """
        # We do not use 'self' for a long while, maybe this kind of dialogueing
        # can be made more OO, but I' m lacking experience in that.
        start_screen  = "\n\t--------------------------------"
        start_screen += "\n\tArgon One fan speed configurator"
        start_screen += "\n\t--------------------------------"
        start_screen += "\n\tWarning: this will remove existing configuration."
        start_screen += "\n\tPress Y to continue:\n"
        start_screen += "\n\t[input]> "

        reply = input(start_screen)
        logger.debug("Reply = '%r'" % reply)

        valid_config_method = None
        if reply.lower() != 'y':

            logger.info("Quitting")
            return
        else:
            logger.debug("Continue")
            valid_config_method = False

        modes = [
            ['1', "Always on",           [100,   []]],
            ['2', "Adjust to temperatures (55, 60, and 65C)",
                                                [0, [55, 60, 65]]],
            ['3', "Adjust to temperatures (35, 50, and 65C)",
                                                [0, [35, 50, 65]]],
            ['4', "Customize behavior",  [0,     None]],
            ['5', "Cancel",              None        ],
        ]
        config_modes = [ config_item(label=mode[0],
                                    desc=mode[1],
                                    sequence=mode[2]) for mode in modes]

        fan_mode_selection = """
        Select fan speed configuration:
        """
        for m in config_modes:
            fan_mode_selection += "\n\t\t%s: %s" % (m.label, m.desc)

        while valid_config_method is False:
            want_mode = input(fan_mode_selection +
                                "\n\tNOTE: You can also " +
                                "edit '{}' directly.".format(config_file) +
                              "\n\t[input]> " )
            for m in config_modes:
                if want_mode == m.label:
                    selected_config = m
                    valid_config_method = True

        sequence = selected_config.sequence

        if sequence is None:
            logger.debug("Cancelled at selector.")
            return None
        else:
            # Init with default speed.
            default_sp, temps_list = sequence

            if isinstance(temps_list, list):
                new_fan_speeds = self.config_ask_inputs(default_sp,
                                                         temps_list[::-1])
            else:
                new_fan_speeds = self.config_ask_inputs(default_sp)
        self.replace_fan_config(new_fan_speeds.get_fan_speeds())

        if 'i2c_host' not in self.config:
            self.add_i2c_host()
        return new_fan_speeds

    def config_get_valid_fan_speed(self, with_temp):
        """
        Dialog that only accepts valid Fan speed values.

        Used in config_ask_inputs.
        """
        while True:
            reply = input("For t = %.1f C, " % with_temp
                          + " speed \n([input 0-100 only]>")

            fan_speed = test_range(reply, int, 0, 100)
            if fan_speed is not False:
                return fan_speed
            else:
                logger.error( "'{}'".format(reply)
                      + " is no valid fan speed, try again.")

    def config_ask_inputs(self, default_fan_speed, t_list=None):
        """
        Ask for fan speeds,if t_list None, ask for temperatures also.
        """
        new_fan_speeds = temp_fan_pairs(default_fan_speed)
        if t_list is None:
            print("Please provide fan speeds and temperature pairs.")
        else:
            t_list.sort(reverse=True)
        add_more = True
        while add_more is True:
            invalid = True
            while invalid is True:
                if t_list is None:
                    new_input = input(
                            "Provide temperature (in Celsius), "
                            + "\n\t'f' to finish or"
                            + "\n\t'p' to print the proposed upto now."
                            + "\n[input?]:>")
                    if new_input.lower() == 'f':
                        add_more = False
                        invalid = False
                        break

                    elif new_input.lower() == 'p':
                        print(new_fan_speeds.fmt_current_config())
                        continue
                    else:
                        pass
                else:
                    # a list was parsed.
                    if len(t_list) < 1:
                        add_more = False
                        invalid = False
                        break
                    else:
                        new_input = t_list.pop()

                new_temp = test_range(new_input, float, 0.0, 100.0)
                if new_temp is not False:
                    invalid = False
                else:
                    logger.warning("The input %r " % new_input
                        + "is not a  valid temperature, try again.")
            if add_more is False:
                self.end_cfg_dialog = True
                break

            new_fan_speed = self.config_get_valid_fan_speed(new_temp)

            logger.info("Adding %.1f C => %2d %% " % (new_temp,
                                                    new_fan_speed))

            new_fan_speeds.add_pair(new_temp, new_fan_speed)
        return new_fan_speeds


class temp_fan_pairs():
    """
    Provision to ensure a proper list:
        * Take care that the temperaturese are ascending.
        * Prevent duplicates by replacing old values with new.
        * Start up with an explicit defined default value.
    """
    def __init__(self, default_speed=0):
        """
        Initialise the temp_fan_pairs with a default, startup value for te fan
        speed.
        """
        self.start_pair = ['default', default_speed]
        self.more_fan_speeds = []

    def add_pair(self, temp, speed):
        # remove possible duplicate old temp value:
        self.more_fan_speeds = [[t, s] for t, s in self.more_fan_speeds
                                                        if t != temp]
        self.more_fan_speeds.append([float(temp), int(speed)])

        self.sort_more_fan_speeds()

    def sort_more_fan_speeds(self):
        self.more_fan_speeds.sort(key=lambda x: x[0])

    def get_fan_speeds(self):
        self.sort_more_fan_speeds()
        return [self.start_pair] + self.more_fan_speeds

    def fmt_current_config(self):
        """
        Return a basical formatted string representation of the configured
        values.
        """

        mesg = "\n\tTemp, Speed"
        mesg += "\n\t----- -----"

        for t, s in self.get_fan_speeds():
            temp = "%r" % t if type(t) != float else "%.1f C\t" % t
            mesg += "\n\t%s\t => %d " % (temp, s)
        return mesg


def main(args):
    """
    Code to test the user interaction.
    """
    cf = configuration()
    cf.i2c_host_configuration_dialog()
    the_config = cf.fan_configuration_dialogs()

    if the_config is None:
        logger.info("Cancelled" )
    else:
        logger.info(the_config.fmt_current_config())

    return 0


if __name__ == '__main__':
    # This is the main body of this module. Ideally it should only contain at
    # most **one** call to the entry point of the program.
    sys.exit(main(sys.argv))
