#!/usr/bin/python3

import os
import sys
import gpiod

from libargonone.do_logging import logger


def event_time(event):
    """
    Get the float value of the event time.
    """
    secs = event.sec * 1.0
    nsecs = event.nsec * 1e-9
    return secs + nsecs


def do_reboot():
    os.system('reboot &')


def soft_shutdown():
    os.system("halt -p &")


def run_button_daemon(*args):
    print(*args)
    bd = button_daemon()
    bd.power_button_check()


class button_daemon():
    """
    Do the power button shutdown check. The hard power down is 'hardwired',
    so we  need to impement the reboot and soft shutdown.
    These are also (re-) encoded by the electronics as oulses of different
    lengths to gpio4.

    - Two brief presses result in a 20ms pulse.
    - A press of 3-5 seconds translaets toa 40ms pulse.
    - The press of 5+ seconds has the electronics cut the power.
    """
    def __init__(self, caller=None, my_gpio_chip='gpiochip0', shutdown_pin=4):
        self.chips={}
        self.claim_pwr_line(caller, my_gpio_chip, shutdown_pin)

    def claim_pwr_line(self, caller=None, chip='gpiochip0', line=4):
        """
        Claim the gpio line triggered by the power button's electronics..
        """
        caller = sys.argv[0] if caller is None else caller
        logger.debug("Accessing IO-chip {}.".format(chip))
        chip = gpiod.Chip(chip)
        self.chips[chip, line] = chip
        self.pwr_line = chip.get_line(line)
        self.pwr_line.request(caller, type=gpiod.LINE_REQ_EV_BOTH_EDGES)
        #self.pwr_line.my_chip = chip

    def release_pwr_line(self):
        """
        Release the power button controlled gpio line.
        """
        self.pwr_line.release()

    def power_button_check(self):
        start_time = None
        self.pwr_check_active = True
        logger.debug("Start power button event loop." )

        while self.pwr_check_active is True:
            events = self.pwr_line.event_read_multiple()[::-1]
            while len(events) > 0:
                event = events.pop()
                if event.type == gpiod.LineEvent.RISING_EDGE:
                    start_time = event_time(event)
                elif event.type == gpiod.LineEvent.FALLING_EDGE:
                    end_time = event_time(event)
                    if start_time is None:
                        logger.error(
                            "The start_time is None. Looks like a "
                            + "FALLING_EDGE occurred before the "
                            + "start_time was set by a RISING_EDGE." )
                    else:
                        logger.debug("Calling shutdown action." )
                        self.shutdown_action(start_time, end_time)

        self.release_pwr_line()


    def shutdown_action(self, rise_time, fall_time):
        """
        Implements the shutdown action based on the pulse length.
        """
        pulse_length = fall_time - rise_time
        # The designed lengths:
        reboot_l = 20e3
        shutdown_l = 40e3
        # Set the distinction in the middle (0.5) of both lengths.
        distinction_l = reboot_l + 0.5 * (shutdown_l - reboot_l)
        if pulse_length < distinction_l:
            logger.warning("Power button call for reboot.")
            do_reboot()
        else:
            # Third; hard shutdown, option is implemented in hardware, so we don't
            # bother about that here.
            logger.warning("Power button call soft shutdown.")
            soft_shutdown()
        self.pwr_check_active = False

def main(args):
    from threading import Thread
    try:
        t1 = Thread(target=run_button_daemon)
        t1.start()

    except Exception as e:
        logger.error("Execption {} detected.".format(e)
                     + " Trying to quit." )
        t1.stop()


if __name__ == '__main__':
    sys.exit(main(sys.argv))
