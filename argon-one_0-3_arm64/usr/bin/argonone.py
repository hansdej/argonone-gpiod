#!/usr/bin/python3

import os
import sys
import argparse

from libargonone.do_logging import logger
from libargonone.bus_control import fan_daemon
from libargonone.power_button import run_button_daemon
from libargonone.do_config import configuration
from libargonone.bus_control import cut_power


def run_fan_configurator(i2c_also=False):
    cf = configuration()
    if i2c_also is True:
        cf.i2c_host_configuration_dialog()
    the_config = cf.fan_configuration_dialogs()

    if the_config is None:
        logger.info("Cancelled configuration" )
    else:
        logger.info("Wrote configuration:\n{}".format(
                    the_config.fmt_current_config()))
        cf.replace_fan_config(the_config.get_fan_speeds())
        cf.write_config()
    return 0


def cut_pwr():
    cut_power()


def run_threads(args):
    """
    Run the deamons as threads.
    """
    from threading import Thread

    t1 = Thread(target=fan_daemon)
    t2 = Thread(target=run_button_daemon)

    for t in t1, t2:
        try:
            t.start()
        except Exception as e:
            logger.error("While starting {}".format(t)
                    + "Exception {} occurred.".format(e)
                    + "\nWill try to stop the process.")
            try:
                t.stop()
            except Exception as e:
                logger.critical("Got exception {}".format(e)
                            + " in the {} ".format(t)
                            + "stop attempt.")


def main(*args, **kwargs):
    if 'daemon' in args:
        run_threads(args)
    elif "configurator" in args:
        run_fan_configurator()
    elif "and_i2c_host" in args:
        run_fan_configurator(i2c_also=True)
    elif "cut_power" in args:
        cut_pwr()
    else:
        logger.warning("\nCall as {}".format(*sys.argv)
                    + " has no further implementation." )


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    exec_modes = parser.add_mutually_exclusive_group()

    exec_modes.add_argument('-D', '--daemon',
                        help='Run the fan and/or button daemon.',
                        action='store_true' )

    exec_modes.add_argument('-c', '--configure',
                        help='Start the configuration interface.',
                        action='store_true' )

    exec_modes.add_argument('-a', '--and_i2c_host',
                        help='Start the configuration interface WITH i2c host'
                        + ' configuration.',
                        action='store_true' )

    exec_modes.add_argument('-k', '--cut_power',
                        help='Cut the case power..',
                        action='store_true' )


    parser.add_argument('-v', '--verbose',
                        help='Show all debugging messages.',
                        action='store_true' )

    cliargs = parser.parse_args()

    exec_mode = None
    if cliargs.daemon or 'argononed' in sys.argv[0]:
        exec_mode = 'daemon'
    elif cliargs.configure or 'argonone-config' in sys.argv[0]:
        exec_mode = 'configurator'
    elif cliargs.and_i2c_host:
        exec_mode = 'and_i2c_host'
    elif cliargs.cut_power:
        exec_mode = 'cut_power'
    else:
        parser.print_help()

    if cliargs.verbose:
        import logging
        logger.setLevel(logging.DEBUG)

    main(exec_mode)
